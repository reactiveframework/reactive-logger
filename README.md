# ReactiveLogger

**TODO: Add description**

## Installation

If [available in Hex](https://hex.pm/docs/publish), the package can be installed as:

  1. Add reactive_logger to your list of dependencies in `mix.exs`:

        def deps do
          [{:reactive_logger, "~> 0.0.1"}]
        end

  2. Ensure reactive_logger is started before your application:

        def application do
          [applications: [:reactive_logger]]
        end

