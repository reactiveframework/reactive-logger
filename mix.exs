defmodule ReactiveLogger.Mixfile do
  use Mix.Project

  def project do
    [app: :reactive_logger,
     version: "0.0.1",
     elixir: ">= 1.2.3",
     build_embedded: Mix.env == :prod,
     start_permanent: Mix.env == :prod,
     deps: deps]
  end

  # Configuration for the OTP application
  #
  # Type "mix help compile.app" for more information
  def application do
    [applications: [:logger],
     mod: {ReactiveLogger, []}]
  end

  # Dependencies can be Hex packages:
  #
  #   {:mydep, "~> 0.3.0"}
  #
  # Or git/path repositories:
  #
  #   {:mydep, git: "https://github.com/elixir-lang/mydep.git", tag: "0.1.0"}
  #
  # Type "mix help deps" for more examples and options
  defp deps do
    [
      { :reactive_api, git: "git@bitbucket.org:ScalableEngineering/reactive-api.git" },
      { :reactive_session, git: "git@bitbucket.org:ScalableEngineering/reactive-session.git" },
      { :reactive_entity, git: "git@bitbucket.org:ScalableEngineering/reactive-entity.git"},
      { :confex, "~> 1.4.1" }
    ]
  end
end
