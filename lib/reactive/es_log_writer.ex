defmodule Reactive.EsLogWriter do
  use Reactive.TemporaryEntity
  require Logger

  def init(args=[url]) do
    Logger.info("Logger writer created #{inspect args}")
    parsed = URI.parse(url)
    {:ok, %{
      url: url,
      protocol: parsed.scheme,
      index: String.slice(parsed.path,1,1000),
      host: parsed.host,
      port: parsed.port,
      hostport: "#{parsed.host}:#{inspect parsed.port}",
      userpass: parsed.userinfo,
      buffer: [],
      buffer_size: 0,
      last_write: 0,
      write_timer: :null
    }, %{}}
  end

  def request({:add_source, name}, state, _from, _rid) do
    observe([Reactive.LogChannel,name],:log)
    { :reply, %{ type: :added }, state  }
  end

  def request({:remove_source, name}, state, _from, _rid) do
    unobserve([Reactive.LogChannel,name],:log)
    { :reply, %{ type: :removed }, state }
  end

  def notify(_from, :log, message, state) do
    #Logger.debug("BUFFER LOG")
    nstate = %{ state | buffer: [ message | state.buffer ], buffer_size: state.buffer_size+1 }
    try_write(nstate)
  end

  def event(:write_timeout,state,_) do
    #Logger.debug("WRITE TIMEOUT!")
    try_write(state)
  end

  def can_freeze(_state, _observed) do
    false
  end

  def try_write(state) do
    #Logger.debug("TRY WRITE!")
    ts = Reactive.Entity.timestamp()
    if(ts > state.last_write + 1000 || state.buffer_size > 128 ) do
      do_write(state, ts)
    else
      case state.write_timer do
        :null ->
          ntimer = :erlang.send_after(state.last_write+1100-ts,self(),{:event,:write_timeout,self()})
          %{ state | write_timer: ntimer }
        _t -> state
      end
    end
  end

  def do_write(state,ts) do
    #Logger.debug("DO WRITE!")
    case state.write_timer do
      :null -> 0
      t -> :erlang.cancel_timer(t)
    end

    send_to_es(state.buffer,state)

    %{state | write_timer: :null, buffer: [], buffer_size: 0, last_write: ts }
  end

  def send_to_es(buffer,state) do
    Task.start(fn() ->
      #Logger.debug("SEND TASK")
      url = "#{state.protocol}://#{state.hostport}/#{state.index}/_bulk"
      json =
        buffer
        |> Enum.map(fn({type,message}) -> "#{:jsx.encode(%{ index: %{ _type: type } }) }\n#{:jsx.encode(message)}\n" end)
        |> Enum.join("")

      #Logger.debug("SEND JSON #{json}\n  TO #{url}")

      auth = "Basic " <> :base64.encode(state.userpass)
      headers = [
        {"Content-Type", "application/json"},
        {"Authorization", auth}
      ]

      case HTTPoison.post(url, json, headers, [connect_timeout: 50000, recv_timeout: 50000, timeout: 50000]) do
        {:error, err} -> Logger.error("LOG SAVE ERROR #{inspect err}")
        {:ok, resp} -> Logger.debug("LOG SAVE RESP #{inspect resp}")
      end
    end)
  end
end