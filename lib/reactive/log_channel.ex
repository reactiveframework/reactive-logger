defmodule Reactive.LogChannel do
  use Reactive.TemporaryEntity
  require Logger

  def init(args = [name]) do
    Logger.info("Log channel created #{inspect args}")
    save_me()
    {:ok,%{
      last_log_ts: Reactive.Entity.timestamp(),
      name: name
    }, %{}}
  end

  def observe(:log, state, _pid) do
    {:ok, state }
  end

  def request({:log, message}, state, _from, _rid) do
    ts = Reactive.Entity.timestamp()
    notify_observers(:log, {state.name, Map.put(message, :ts, ts)})
    { :reply, :ok , %{ state | last_log_ts: ts } }
  end

  def request({:api_request,[:log,message],contexts} ,state,_from,_rid) do
    ts = Reactive.Entity.timestamp()
    session_id = Map.get(contexts,:session_id)
    notify_observers(:log, {state.name, Map.put(Map.put(message, :ts, ts), :session, session_id)})
    { :reply, %{ type: :added, ts: ts }, %{ state | last_log_ts: ts } }
  end

  def event({:log, message}, state, _from) do
    ts = Reactive.Entity.timestamp()
    notify_observers(:log, {state.name, Map.put(message, :ts, ts)})
    %{ state | last_log_ts: ts }
  end

  def can_freeze(_state, _observed) do
    false
  end

end