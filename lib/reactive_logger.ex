defmodule ReactiveLogger do
  use Application
  require Logger

  # See http://elixir-lang.org/docs/stable/elixir/Application.html
  # for more information on OTP Applications
  def start(_type, _args) do
    import Supervisor.Spec, warn: false

    outputs = Application.get_env(:reactive_logger, :outputs)

    Enum.each(outputs, fn(output) ->
      output = Confex.process_env(output)
      case output[:type] do
        :es ->
          id = [Reactive.EsLogWriter, output[:url]]
          Enum.each(output[:include], fn(include) ->
            Reactive.Entity.request(id,{:add_source, include})
          end)
      end
    end)

    children = [
      # Define workers and child supervisors to be supervised
      # worker(ReactiveLogger.Worker, [arg1, arg2, arg3]),
    ]

    # See http://elixir-lang.org/docs/stable/elixir/Supervisor.html
    # for other strategies and supported options
    opts = [strategy: :one_for_one, name: ReactiveLogger.Supervisor]
    Supervisor.start_link(children, opts)
  end
end
